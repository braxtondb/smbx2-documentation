# Mount constants

Constants for mounts and mount types.

## Mount Type

Values for the player.mount field.

| Constant | Value | Description |
| --- | --- | --- |
| MOUNT_NONE | 0 | No mount. |
| MOUNT_BOOT | 1 | Any boot. |
| MOUNT_CLOWNCAR | 2 | Clown car. |
| MOUNT_YOSHI | 3 | Any Yoshi. |

## Boot Color

Values for the player.mountColor field while in a boot.

| Constant | Value | Description |
| --- | --- | --- |
| BOOTCOLOR_GREEN | 1 | Kuribo's (regular) Shoe. |
| BOOTCOLOR_RED | 2 | Podoboo's (lavaproof) Shoe. |
| BOOTCOLOR_BLUE | 3 | Lakitu's (flying) Shoe. |

## Yoshi Color

Values for the player.mountColor field while riding Yoshi.

| Constant | Value | Description |
| --- | --- | --- |
| YOSHICOLOR_GREEN | 1 | Green (regular) Yoshi. |
| YOSHICOLOR_BLUE | 2 | Blue (flying) Yoshi. |
| YOSHICOLOR_YELLOW | 3 | Yellow (stomping) Yoshi. |
| YOSHICOLOR_RED | 4 | Red (fire-breathing) Yoshi. |
| YOSHICOLOR_BLACK | 5 | Black (flying, stomping, fire-breathing) Yoshi. |
| YOSHICOLOR_PURPLE | 6 | Purple (sround-pounding) Yoshi. |
| YOSHICOLOR_PINK | 7 | Pink (vegetable-breathing) Yoshi. |
| YOSHICOLOR_CYAN | 8 | Cyan (ice) Yoshi. |