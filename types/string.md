# String

Strings are reference types that contain text. They are usually denoted using single- or double quotes. You should always use one consistently in your code. Use the other when a string has quotes within.

```lua
local dialogue = "Hello! Nice to meet you."
local dialogue2 = "She said: 'What's the deal? Huh?'"
```

## Concatenation

Strings can be joined with numbers and strings using the concatenation operator.

```lua
local watermelonCount = 50
local familyMember = "sister"
local dialogue = "Today, I will purchase " .. watermelonCount .. " watermelons for my " .. familyMember .. "!"
```