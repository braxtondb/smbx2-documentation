# NPC AI

This page includes descriptions of the AI of all basegame NPCs with special behaviour. They can be accessed as part of a NPC instance of the given ID. This page is likely incomplete.

## 1-100

#### 3 (Red Paragoomba)

<Badge type="tip">ai1</Badge> Increments 1 per tick, at 32 the jumps are starting and it only increments one per jump until 35.

#### 3, 7, 24, 73, 113, 114, 115, 116, 172, 174 (Shells)

<Badge type="tip">ai4</Badge> Timer counts up from 0 to 3 when changing direction and spawning hit effect.

#### 8, 93 (Rightside-up Piranha Plant)

<Badge type="tip">ai1</Badge> Timer (increments to a different thereshold based on ai2)<br>
<Badge type="tip">ai2</Badge> State (1=showing up [ai1 0-32], 2=showing [ai1 0-49], 3=going down [ai1 0-32], 4=hiding [ai1 33-74])

#### 10 (Coin) (potentially all other coins too?)

<Badge type="tip">ai1</Badge> 1 if gravity should take effect

#### 12 (Podoboo (Deprecated))

<Badge type="tip">ai1</Badge> Timer<br>
<Badge type="tip">ai2</Badge> State (1 = jumping, 2 = falling)

#### 13 (Player Fireball)

<Badge type="tip">ai1</Badge> State (1-5 corresponding to the fireball style of the first 5 player characters)<br>
<Badge type="tip">ai2</Badge> 1 if Peach's fireball bounced off a wall

#### 15 (Boom Boom)

<Badge type="tip">ai1</Badge> State (0 = running, 1 = spiked, 2 = transition to spiked, 3 = jumping, 4 = hurt)<br>
<Badge type="tip">ai2</Badge> Timer

#### 21 (Bill Blaster)

<Badge type="tip">ai1</Badge> Timer: Increments 1 per tick, at 200 one bullet bill is fired unless the player is over or underneath the blaster. (incremeting will not be paused!)

#### 22 (Billy Gun)

<Badge type="tip">ai1</Badge> Timer: Increments 10 per tick when the player is holding the billy gun, at 200 one bullet bill is fired.

#### 25 (SMB2 Ninji)

<Badge type="tip">ai1</Badge> Timer: Decrements from 5 to 0 while grounded. When the value is 0 then the ninji jumps.

#### 28 (Cheep Cheep) (other cheep cheeps too?)

<Badge type="tip">ai1</Badge> Selected AI behaviour (starting at 0, in oder or Editor options)<br>
<Badge type="tip">ai4</Badge> 0/1 for swimming up/down

#### 29 (SMB1 Hammer Bro)

<Badge type="tip">ai1</Badge> Direction timer: Sign corresponds to direction, resets roughly every 100 ticks.<br>
<Badge type="tip">ai2</Badge> Jump timer: Increments 1 per tick (while not shooting), if reaches 250 then super-jump is executed and the timer resets<br>
<Badge type="tip">ai3</Badge> Throwing releated. High number = more throws

#### 34 (Leaf)

<Badge type="tip">ai1</Badge> Timer: From 1 to 6. Controls the "swing"-effect for correct speed. When the value 6 is reached the direction of this swing is switched.

#### 37 (Thwomp (Deprecated))

<Badge type="tip">ai1</Badge> State (0=idle, 1=falling, 2=down, 3=retreating)<br>
<Badge type="tip">ai2</Badge> Timer: For state 2 => from 0 to 100; When hit value 100 then state is switched to 3.

#### 38, 43, 44 (Boo)

<Badge type="tip">ai1</Badge> State (0=idle, 1=follow)

#### 39 (Birdo)

<Badge type="tip">ai1</Badge> State (0=movement, 1=fire, or -30 to 0 if got hit)<br>
<Badge type="tip">ai2</Badge> Timer: Increments 1 per tick; if reaches 125 then jump, if reaches 250 then shoot, at 280 resets to 0<br>
<Badge type="tip">ai3</Badge> Movement timer: Increments 1 per tick, NPC changes direction when the timer hits 500

#### 45 (Throw Block)

<Badge type="tip">ai1</Badge> State (0=inactive, 1=active)<br>
<Badge type="tip">ai2</Badge> Timer: The block disappears when it reaches 450, or when the timer is 0 and the block collides with a wall.

#### 46, 212 (Donut Block)

<Badge type="tip">ai1</Badge> State (0=inactive, 1=active)<br>
<Badge type="tip">ai2</Badge> 1 if stood on<br>
<Badge type="tip">ai3</Badge> Timer: Increments when ai2 == 1. 0-5 for red, 0-30 for brown. Toggles ai1 when limit is reached.

#### 47 (SMB3 Lakitu)

<Badge type="tip">ai1</Badge> Travel direction<br>
<Badge type="tip">ai2</Badge> 0 when sinking, 1 when rising<br>
<Badge type="tip">ai3</Badge> State: (0=bobs down, 1=bobs up, 2=telegraph, 3=throw)<br>
<Badge type="tip">ai4</Badge> Preparing to throw timer: Resets if at 0 when it reaches the player, starts from 100 and decreases by 1 each frame.<br>
<Badge type="tip">ai5</Badge> Throw timer: Starts at 0, will not start counting until ai4 is also 0. Increases by 1 every 6 ticks up to 20, then a Spiny Egg is thrown and the timer resets (but does not begin counting yet).

#### 49 (Toothy Pipe)

<Badge type="tip">ai1</Badge> State (0=idle, 1=held)<br>
<Badge type="tip">ai2</Badge> NPC Array Index of Toothy NPC (NOTE: This number being high may be cause for lag)

#### 50 (Toothy)

<Badge type="tip">ai1</Badge> Stays 1, if set to 0 then Toothy despawns

#### 51 (Upside-down piranha plant)

<Badge type="tip">ai1</Badge> Timer (increments to a different thereshold based on ai2)<br>
<Badge type="tip">ai2</Badge> State (1=showing up [ai1 0-42], 2=showing [ai1 0-49], 3=going down [ai1 0-42], 4=hiding [ai1 43-109])

#### 52 (Horizontal piranha plant)

<Badge type="tip">ai1</Badge> Timer (increments to a different thereshold based on ai2)<br>
<Badge type="tip">ai2</Badge> State (1=showing up [ai1 0-32], 2=showing [ai1 0-49], 3=going down [ai1 0-32], 4=hiding [ai1 33-109])

#### 54 (Fighterfly)

<Badge type="tip">ai1</Badge> Timer: From 0 to 29. When reached value 29 then the jump is beeing made.<br>

#### 60, 62, 64, 66 (Switch Platform)

<Badge type="tip">ai1</Badge> SpeedX<br>
<Badge type="tip">ai2</Badge> SpeedY<br>
<Badge type="tip">ai5</Badge> ID of line guide attached to. 0 if none.

#### 74 (Piranhacus Giganticus)

<Badge type="tip">ai1</Badge> Timer (increments to a different thereshold based on ai2)<br>
<Badge type="tip">ai2</Badge> State (1=showing up [ai1 0-42], 2=showing [ai1 0-49], 3=going down [ai1 0-42], 4=hiding [ai1 43-74])

#### 75 (Jumping Toad)

<Badge type="tip">ai1</Badge> Timer: [Tick 0-9 Anticipation], [Tick 0-9 Jumping], When falling down timer stays at value 100

#### 76, 121, 122, 123, 124 (Parakoopa) (Flying NPC AI)

<Badge type="tip">ai1</Badge> Selected AI behaviour (starting at 0, in oder or Editor options)<br>
<Badge type="tip">ai3</Badge> For Hover left/right: Direction flag for up/down movement

#### 84 (SMW Bowser Statue (Deprecated))

<Badge type="tip">ai1</Badge> Increments 1 per tick, resetting when it shoots (shooting about Tick ~220 [Not fixed])

#### 86 (SMB3 Bowser)

<Badge type="tip">ai1</Badge> Resets to 9 when starting to jump high and decrements 1 per tick. Not sure but may start arcing after it hits 0<br>
<Badge type="tip">ai2</Badge> Timer used to determine what attack phase Bowser does; increases randomly by 0 to 1 per tick.<br>
<Badge type="tip">ai3</Badge> Timer used for Bowser's attack phases.<br>
<Badge type="tip">ai4</Badge> State (2=hopping, 3=jumping high+falling, 4=shooting fireballs, 10=turning right after hitting ground, -10=turning left after hitting ground)<br>
<Badge type="tip">ai5</Badge> Targeted player. (When 0, Bowser looks to target a new player. Is 0 after ground pounding.)

#### 91 (Pluckable Grass)

<Badge type="tip">ai1</Badge> Increments 1 per tick, resetting when it shoots (shooting about Tick ~220 [Not fixed])

#### 95, 98, 99, 100, 148, 149, 150, 228 (Yoshi)

<Badge type="tip">ai1</Badge> 1 when hurt/running<br>
<Badge type="tip">ai2</Badge> Counter related to head movement when hurt

#### 96 (Yoshi Egg)

<Badge type="tip">ai1</Badge> The ID of the NPC contained within the egg

#### 97 (SMB3 Star)

<Badge type="tip">ai1</Badge> 1 if the star was already collected<br>
<Badge type="tip">ai2</Badge> 0 if moving up, 1 if moving down<br>
<Badge type="tip">ai4</Badge> Direction Flag Timer (From 0 to 4; When 4 reset to 0 and toggle the Direction Flag)

## 101-200

#### 104 (SMB3 Wood Platform)

<Badge type="tip">ai1</Badge> SpeedY<br>
<Badge type="tip">ai2</Badge> SpeedX

#### 105 (SMW Sinking Platform)

<Badge type="tip">ai1</Badge> 1 if falling/stood on by the player

#### 126, 127, 128 (LoZ Bit/Bot)

<Badge type="tip">ai1</Badge> AI-State (2 = Moving [ai2 0-119], 1 = Charging [ai2 0-89 or 29], -1 = Jumping [ai2 stays at 0 until reaching the floor]). Note: The AI States are quite random.<br>
<Badge type="tip">ai2</Badge> Timer

#### 129 (Tweeter)

<Badge type="tip">ai1</Badge> Jump Timer: 0 to 3, on every jump this timer increases by 1 until it reaches value 3. Then the next jump is a high jump and the timer resets to 0.